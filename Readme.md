# Ycrawler
асинхронный краулер для новостного сайта [news.ycombinator.com](https://news.ycombinator.com)

+ краулер сохраняет отдельные треды в каталоги с соответсвующими идентификаторами
+ помимо самой страницы треда, в каталог сохраняются все внешние ссылки из комментариев
+ при запуске выкачиваются первые 30 тредов
+ краулер должен переодически проверять наличие новых тредов и инициировать их закачку
+ только стандартная библиотека и `aiohttp`

## Зависимости
+ python3.7+
+ aiohttp

## Использование
```
pip install aiohttp
python3.7 -Wd  -m unittest discover
python3.7 -m hackerscraper
```

## Особенности
+ Приложение настраивается только через переменные в файле __main__.py
+ Действия логируются в файл crawler.log
+ Новость сохраняется в файл `PATH_PREFIX/{news_id}/index.html`, рядом
    складываются ссылки из комментариев.
+ Все урлы собираются в одну очередь как инстанс неймед туплы `DownloadTask`:
    урл и идентификатор его принадлежности.
+ По завершению обработки очереди кравлер останавливается. IE в результате
    кравлер является только квазипериодичным.
+ Если указать параметр `PROCESSING_TIMEOUT = 0`, то на время обработки очереди
    не накладывается ограничение и с периодичностью слипа урлы постоянно добавляются для
    обработки
+ Состояние обработанных урлов не сохраняется в рантайме, обработка треда
    отменяется если существует связанная с ним дирректория
+ Следствие предыдущего: между каждыми циклами сна, кравлер полностью парсит
    главную страницу.
+ Сторонние ссылки бесцеремонно игнорируются если при их скачивании произошла
    ошибка, если статус код ответа не 200 или если предоставляемый
    контекст-тайп не текст.
+ Парсинг сделан на основе нативного `HTMLParser` c относительно наивными
    допущениями.
+ Юзерагент для кравлера никак не настроен, из-за этого некоторые урлы могут
    отдавать 403.
+ `aiohttp` спаимт SSL ошибками <3
