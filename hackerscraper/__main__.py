"""
Haker news crawler.
Download top 30 news and fetch anytign new
"""
import os
import re
import asyncio
import logging

from contextlib import asynccontextmanager
from collections import namedtuple
from urllib.parse import urlparse

import aiohttp

from .parsers import extract_item_urls, extract_thread_urls


# Helper class to store url and reason it exists
DownloadTask = namedtuple("DownloadTask", "url parent")
HACKER_URL = "https://news.ycombinator.com/"
TEST_ITEMS = {
    "item?id=20541292",
    "item?id=20537941",
    "item?id=20540379",
    "item?id=20534211",
    "item?id=20539978",
    "item?id=20539867",
}
# Prefix for filestree where to store data
PATH_PREFIX = "items"
# Delay before repeating processing of root page
DELAY = 100
# Allow main processing for 10 minutes
PROCESSING_TIMEOUT = 10 * 60
WORKERS_COUNT = 5
# How many retries to do is hope of getting 200 response code
RETRY_COUNT = 10
RETRY_DELAY = 4

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
logging.basicConfig(
    filename="crawler.log",
    format="[%(asctime)s] %(levelname)s %(filename)s:%(lineno)-5d %(message)s",
    datefmt="%Y.%m.%d %H:%M:%S",
)
item_id_mask = re.compile(r"^https:\/\/.*item\?id=([0-9]*)$")


def get_test_urls():
    """ Helper function to insert adhoc thread urls """
    return [
        DownloadTask(url=HACKER_URL + item, parent=None) for item in TEST_ITEMS
    ]


def save_text_file(filename, text):
    """ Naive function to save string into file with given filename """
    with open(filename, "w") as fp:
        fp.write(text)


def url_validator(url):
    """ Basic url validator: if urlib is able to detect scheme and netloc then
    we are fine and be happy to postpone sudden failures
    """
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


@asynccontextmanager
async def get_url_with_retry(
    url, session, retry_count=5, delay=5, timeout_total=60
):
    """ Snippet for usage of session.get() with retry_count until response code
    isn't 200. Return response isntance with text() cached into _text attribute
    """

    LOG.info(f"Fetch url {url}")
    timeout = aiohttp.ClientTimeout(total=timeout_total)
    attempt = 1
    while True:
        async with session.get(url, timeout=timeout) as response:
            if response.status != 200:
                await asyncio.sleep(delay)
            else:
                response._text = await response.text()
                break
        attempt += 1
        if attempt > retry_count:
            LOG.error(f"Failed to fetch url {url}")
            raise RuntimeError
    yield response


async def fetch_external_link(url, path, session):
    """ Handler for external links. Download it, store into path as file with
    filename build from url, by replacing alphanumeric characters with
    underscore.
    Url is processed as is, if response status isn't 200 do notging. Result saved only for
    responses with content-type named as 'text/'.
    """
    data = None
    filename = None
    try:
        timeout = aiohttp.ClientTimeout(total=60)
        async with session.get(url, timeout=timeout) as response:
            if response.status != 200:
                LOG.error(
                    f"Failed to download {url}: {response.status} for {path}"
                )
                return
            if response.headers.get("content-type").startswith("text/"):
                base_filename = "".join(
                    (ch if ch.isalnum() else "_" for ch in url)
                )
                filename = os.path.join(path, base_filename)
                data = await response.text()
            else:
                LOG.error(
                    f'Unknown content type {url}: f{response.headers.get("content-type")} for {path}'
                )
    except Exception as exp:
        LOG.error(f"Failed to fetch {url} {repr(exp)} for {path}")
    if filename and data:
        try:
            save_text_file(filename, data)
        except Exception as exp:
            LOG.error(
                f"Failed to save data from {url} into {filename}: {repr(exp)} for {path}"
            )


async def fetch_thread_page(url, session):
    """ Handler for site thread links. Fetch data from page,
    save it into file, return all external links
    """
    try:
        item_id = re.match(item_id_mask, url).groups()[0]
    except (AttributeError, IndexError):
        raise ValueError(f"Not valid {url}")
    path = os.path.join(PATH_PREFIX, item_id)

    try:
        os.makedirs(path)
    except FileExistsError:
        # TODO: decide what to do if directory already exists
        LOG.info(f"{path} exists")
        return []

    async with get_url_with_retry(url, session) as response:
        data = response._text
        save_text_file(os.path.join(path, "index.html"), data)
    # From extract_thread_urls we get set of urls. Convert them into source
    # objects (url, parent_id) and return as list
    dtasks = []
    for url in extract_thread_urls(data):
        if url_validator(url):
            dtasks.append(DownloadTask(url=url, parent=item_id))
    return dtasks


async def worker(name, session, queue):
    """ Core workers to process thread links. Downloand and save data """
    while True:
        source = await queue.get()
        LOG.info(f"processing {source.url}")
        try:
            if source.parent:
                path = os.path.join("items", source.parent)
                if os.path.exists(path):
                    await fetch_external_link(source.url, path, session)
            else:
                dtasks = await fetch_thread_page(source.url, session)
                for dtask in dtasks:
                    queue.put_nowait(dtask)
        except Exception as ex:
            LOG.error(ex)
        finally:
            queue.task_done()


async def update_threads(session, queue):
    # Mock data
    # for url in get_test_urls():
    #     queue.put_nowait(url)
    async with get_url_with_retry(HACKER_URL, session) as response:
        urls = extract_item_urls(response._text)
    for url_suffix in urls:
        url = HACKER_URL + url_suffix
        LOG.info(f"Add {url} into queue")
        queue.put_nowait(DownloadTask(url=url, parent=None))


async def main():
    # Set settings for used aiogttp connector
    # https://docs.aiohttp.org/en/stable/client_advanced.html#connectors
    conn = aiohttp.TCPConnector(
        # don't limit connection pool just for the hell of it
        limit=0,
        # limit number of connections per host to 6
        limit_per_host=6,
    )
    queue = asyncio.Queue()
    async with aiohttp.ClientSession(connector=conn) as session:
        tasks = []
        for i in range(WORKERS_COUNT):
            task = asyncio.create_task(worker(f"worker-{i}", session, queue))
            tasks.append(task)
        try:
            while True:
                await update_threads(session, queue)
                if PROCESSING_TIMEOUT:
                    try:
                        await asyncio.wait_for(queue.join(), timeout=PROCESSING_TIMEOUT)
                    except asyncio.TimeoutError:
                        LOG.error("Timeout error while processing base queue")
                LOG.info(f"sleep for {DELAY}")
                await asyncio.sleep(DELAY)
        finally:
            for task in tasks:
                task.cancel()
        await asyncio.gather(*tasks, return_exceptions=True)


if __name__ == "__main__":
    asyncio.run(main())
