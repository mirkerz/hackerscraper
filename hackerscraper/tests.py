import asyncio

from unittest import TestCase

import aiohttp

from .parsers import extract_item_urls, extract_thread_urls
from .__main__ import get_url_with_retry


class TestParsers(TestCase):
    def test_extract_item_urls(self):
        item = """
            <html>
            <a href='item?id=foo'>Correct link</a>
            <a href='item?id=boo'>Correct link</a>
            <a href='item?id=foo' hidden>Correct link</a>
            <a href='_item?id=foo'>broken link</a>
            </html>
        """
        self.assertEqual(extract_item_urls(item), {"item?id=foo", "item?id=boo"})

    def test_extract_thread_urls(self):
        data = """
            <html>
            <span class='commtext fff'><a href='broken' rel="nofollow">broken</a></span>
            <a href='foo' class='storylink'>Root url</a>
            <span class='commtext fff'><a href='boo' rel="nofollow">Correct link</a></span>
            <span class='commtext fff'><a href='boo1' rel="nofollow">Correct link</a><a href='boo2' rel="nofollow">Correct link</a></span>
            <span class='fff'><a href='broken'>broken link</a></span>
            <a href='broken' rel="nofollow">broken link</a>
            </html>
        """
        self.assertSetEqual(extract_thread_urls(data), {"foo", "boo", "boo1", "boo2"})


class TestAsyncUtils(TestCase):
    def setUp(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(None)

    def tearDown(self):
        self.loop.close()

    def test_get_url_with_retry_context(self):
        """ Test context manager """

        async def async_wrapper(url):
            async with aiohttp.ClientSession() as session:
                async with get_url_with_retry(url, session) as response:
                    self.assertEqual(response.status, 200)
                self.assertTrue(hasattr(response, "_text"))

        self.loop.run_until_complete(async_wrapper("http://example.com/"))

    def test_get_url_with_retry_context_error(self):
        """ Test context manager """

        async def async_wrapper(url):
            async with aiohttp.ClientSession() as session:
                try:
                    kwargs = {
                        "url": "http://example.com/404",
                        "session": session,
                        "retry_count": 2,
                        "delay": 0,
                    }
                    async with get_url_with_retry(**kwargs):
                        pass
                except Exception as ex:
                    self.exception_raised = True
                    self.assertIsInstance(ex, RuntimeError)

        self.exception_raised = False
        self.loop.run_until_complete(async_wrapper("http://example.com/"))
        self.assertTrue(self.exception_raised)
