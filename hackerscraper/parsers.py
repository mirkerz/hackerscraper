from html.parser import HTMLParser


class HackerParser(HTMLParser):
    """ HTML parser to extract all urls from root page """

    urls = set()

    def handle_starttag(self, tag, attrs):
        """ Find tags <a href="item?id=ID">.*</a> """
        if tag != "a":
            return
        if len(attrs) != 1:
            return
        attr, value = attrs[0]
        if attr != "href":
            return
        if not value.startswith("item?id="):
            return
        self.urls.add(value)


class ThreadParser(HTMLParser):
    """ Extract links from thread: post and comments """

    is_comment = False
    main_url = False
    urls = set()

    def check_main_url(self, tag, attrs):
        if not self.main_url and len(attrs) == 2:
            for attr, value in attrs:
                is_root_url = False
                if attr == "class" and value == "storylink":
                    is_root_url = True
                if attr == "href":
                    root_url = value
            if is_root_url:
                self.main_url = root_url
                self.urls.add(root_url)

    def save_comment_url(self, tag, attrs):
        for attr, value in attrs:
            if attr == "href":
                self.urls.add(value)

    def handle_starttag(self, tag, attrs):
        """ Find tags <a class="storylink" href="">.*</a> """
        if tag not in ("span", "a"):
            return
        if not self.main_url and tag == "a":
            self.check_main_url(tag, attrs)
            return
        if self.is_comment and tag == "a":
            self.save_comment_url(tag, attrs)
            return
        if tag == "span" and len(attrs) == 1:
            if attrs[0][0] == "class" and attrs[0][1].startswith("commtext "):
                self.is_comment = True
            return

    def handle_endtag(self, tag):
        if tag == "span" and self.is_comment:
            self.is_comment = False


def extract_item_urls(data):
    """ Take html formated text and return links to all threads """
    parser = HackerParser()
    parser.feed(data)
    return parser.urls


def extract_thread_urls(data):
    parser = ThreadParser()
    parser.feed(data)
    return parser.urls
